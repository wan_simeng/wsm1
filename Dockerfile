FROM composer as composer

WORKDIR /var/www/html
COPY ./src /var/www/html
RUN composer install 

FROM php:7.4-fpm 

RUN docker-php-ext-install pdo pdo_mysql
WORKDIR /var/www/html
COPY --from=composer  /var/www/html /var/www/html
RUN chmod -R 777 storage && chmod -R 777 bootstrap/cache
